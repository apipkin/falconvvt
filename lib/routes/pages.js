module.exports = {
    home: function (req, res) {
        res.render('index', {
           title: req.app.locals.title
        });
    }
};
