var decodeData = {
    '2000' : {
        'BMW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Chevrolet' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Lexus' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Mercedes' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'VW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        }
    },

    '2001' : {
        'BMW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Chevrolet' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Lexus' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Mercedes' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'VW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        }
    },

    '2002' : {
        'BMW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Chevrolet' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Lexus' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Mercedes' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'VW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        }
    },

    '2003' : {
        'BMW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Chevrolet' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Lexus' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Mercedes' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'VW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        }
    },

    '2004' : {
        'BMW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Chevrolet' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Lexus' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Mercedes' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'VW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        }
    },

    '2005' : {
        'BMW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Chevrolet' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Lexus' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Mercedes' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'VW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        }
    }
};


module.exports = {
    decode: function (req, res) {
        var year = req.params.year,
            make = req.params.make,
            model = req.params.model,
            trim = req.params.trim,
            data = decodeData,
            postfix = '.',
            type = 'years';

        if (year) {
            year = year.split('.')[0];
            data = decodeData[year];
            postfix += year.slice(-1);
            type = 'makes';
        }

        if (make) {
            make = make.split('.')[0];
            data = data[make];
            postfix += make.charAt(0);
            type = 'models';
        }

        if (model) {
            model = model.split('.')[0];
            data = data[model];
            postfix += model.charAt(0);
            type = 'trims';
        }

        if (typeof data.length === 'undefined') {
            data = Object.keys(data);
        }

        data.forEach(function (val, index, arr) {
            arr[index] = {
                value: val,
                text: val + postfix
            };


        });

        res.json({
            type: type,
            data: data
        });
    }
};
