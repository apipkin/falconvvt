Falcon.baseApiUrl = '/api';

function trim (str) {
    if (typeof str !== 'string') { return str; }

    if (str.trim) {
        return str.trim();
    } else {
        return str.replace(/^\s+|\s+$/g, '');
    }
}

function constructRequest (urlTemplate, data, exitEarly) {
  var paths = urlTemplate.split('/'),
      _url = [],
      _data,
      i, len,
      path;

  if (typeof exitEarly === 'undefined') {
    exitEarly = true;
  }

  for (i = 0, len = paths.length; i < len; i++) {
    path = paths[i];

    if (path.charAt(0) !== ':') {
      _url.push(path);
      continue;
    }

    // we want to look up the path in the data set
    path = path.substring(1);

    if (path in data) {
      if (typeof data[path] === 'function') {
        _data = data[path](urlTemplate, data, exitEarly);
      } else {
        _data = data[path];
      }
      _url.push(_data);

    } else if (exitEarly) {
      break;
    }
  }

  return _url.join('/').replace(/(\/)(?=\1)/g, '$1');
}

var Decoder = Falcon.Model.extend({
    url: '/decode/:year/:make/:model',

    makeUrl: function (type, parent, id) {
        var ext, parentPeriodIndex, parentSlashIndex, parentUrl, periodIndex, url, _ref1;

        url = (typeof this.url === 'function') ? this.url() : this.url;

        if (typeof url !== 'string') {
            url = "";
        }

        url = trim(url);

        if (typeof type !== 'string') {
            type = "";
        }

        type = type.toUpperCase();

        if (type !== 'GET' && type !== 'PUT' && type !== 'POST' && type !== 'DELETE') {
            type = 'GET';
        }

        if (id === void 0 && (typeof parent === 'string') || (typeof parent === 'number')) {
            _ref1 = [id, parent], parent = _ref1[0], id = _ref1[1];
        }

        parent = parent !== void 0 ? parent : this.parent;
        ext = "";
        periodIndex = url.lastIndexOf(".");

        if (periodIndex > -1) {
            ext = url.slice(periodIndex);
            url = url.slice(0, periodIndex);
        }

        if (url.charAt(0) !== "/") {
            url = "/" + url;
        }

        if (Falcon.isModel(parent)) {
            parentUrl = parent.makeUrl();
            parentPeriodIndex = parentUrl.lastIndexOf(".");
            parentSlashIndex = parentUrl.lastIndexOf("/");

            if (parentSlashIndex < parentPeriodIndex) {
                if (parentPeriodIndex > -1) {
                    parentUrl = parentUrl.slice(0, parentPeriodIndex);
                }
                parentUrl = trim(parentUrl);
            }
            url = "" + parentUrl + url;
        } else if (typeof Falcon.baseApiUrl === 'string') {
            url = "" + Falcon.baseApiUrl + url;
        }

        if (type === "GET" || type === "PUT" || type === "DELETE") {
            if (url.slice(-1) !== "/") {
                url += "/";
            }

            url = constructRequest(url, {
                year: this.year(),
                make: this.make(),
                model: this.model(),
                trim: this.trim()
            });
        }

        url = url.replace(/([^:])\/\/+/gi, "$1/").replace(/^\/\//gi, "/");

        return "" + url + ext;
    },

    observables: {
        year  : null,
        make  : null,
        model : null,
        trim  : null,

        years : [],
        makes : [],
        models: [],
        trims : []
    }
});

var DecoderView  = Falcon.View.extend({
    url: '#decoder-template',

    defaults: {
        decoder: function () {
            return new Decoder;
        }
    },

    observables: {
        ymmt: function () {
            var data = this.decoder.serialize();

            if (this.decoder.trim()) {
                return data.year + ' ' + data.make + ' ' + data.model + ' ' + data.trim;
            } else {
                return 'Need to select YMMT';
            }
        }
    },

    initialize: function () {

        // limit the noise for the select updates
        this.decoder.year.extend({ rateLimit: 2 });
        this.decoder.make.extend({ rateLimit: 2 });
        this.decoder.model.extend({ rateLimit: 2 });
        this.decoder.trim.extend({ rateLimit: 2 });

        this.decoder.sync('GET', {
            complete: this._handleSync
        }, { data: this.serialize() });

        // kick off life cycle
        this._bind();
    },

    serialize: function () {
        return {
            year : this.decoder.year(),
            make : this.decoder.make(),
            model: this.decoder.model(),
            trim : this.decoder.trim()
        };
    },

    _bind: function () {
        // Subscribe to change events
        this.decoder.year.subscribe(this._yearChanged, this);
        this.decoder.make.subscribe(this._makeChanged, this);
        this.decoder.model.subscribe(this._modelChanged, this);
        this.decoder.trim.subscribe(this._trimChanged, this);
    },

    _handleSync: function (self, data, options) {
        if (data.type) {
            self[data.type](data.data);
        }
    },

    _yearChanged: function (newVal) {
        this.decoder.make(null);
        console.log(this.decoder.serialize());

        if (newVal) {
            this.decoder.sync('GET', {
                complete: this._handleSync
            });
        }
    },

    _makeChanged: function (newVal) {
        this.decoder.model(null);

        if (newVal) {
            this.decoder.sync('GET', {
                complete: this._handleSync
            });
        }
    },

    _modelChanged:  function (newVal) {
        this.decoder.trim(null);

        if (newVal) {
            this.decoder.sync('GET', {
                complete: this._handleSync
            });
        }
    },

    _trimChanged: function (newVal) {
        var data = newVal ? this.serialize() : null;

        Falcon.trigger('decoder:updated', data);
    }
});


var Transaction = Falcon.Model.extend({
    defaults: {
        dateSold: '',
        price: '',
        odometer: '',
        condition: '',
        engine: '',
        color: '',
        type: '',
        region: '',
        auction: ''
    }
});

var Transactions = Falcon.Collection.extend({
    model: Transaction
});

var TransactionsView = Falcon.View.extend({
    url: '#trxs-template',

    defaults: {
        transactions: function () {
            return new Transactions();
        }
    },

    observables: {
        page: 1,

        itemsPerPage: 2,

        showingAll: false,

        pageSize: function () {
            return (this.itemsPerPage() <= this.transactions.length()) ?
                this.itemsPerPage() : this.transactions.length();
        },

        hasTransactions: function () {
            return this.transactions.length() > 0;
        },

        viewableTransactions: function () {
            var size;

            if (!this.showingAll() && this.pageSize() < this.transactions.length()) {
                size = this.pageSize();
            }

            return this.transactions.slice(0, size);
        }
    },

    initialize: function () {
        this._dataTable = null;
        this._sort = true;

        // kick off life cycle
        this._bind();
    },

    toggleShowAll: function () {
        this.showingAll(!this.showingAll());
    },

    _bind: function () {
        this.transactions.models.subscribe(this._transactionsChanged, this);

        this.listenTo(Falcon, 'decoder:updated', this._fetchTransactions);
    },

    _fetchTransactions: function (data) {
        var arr = !data ? [] : [
            {
                dateSold: '03/20/2014',
                price: '34500',
                odometer: '10,489',
                condition: '4.9',
                engine: '4GT/P',
                color: 'BROWN',
                type: 'Regular',
                region: 'Northeast',
                auction: 'PA - Manheim Pennsylvania'
            },{
                dateSold: '03/11/2014',
                price: '35000',
                odometer: '14918',
                condition: '2.6',
                engine: '4GT/A',
                color: 'ICE SILV',
                type: ' Lease',
                region: 'Midwest',
                auction: 'IL - Manheim Arena Illinois'
            },{
                dateSold: '03/20/2014',
                price: '34500',
                odometer: '10,489',
                condition: '4.9',
                engine: '4GT/P',
                color: 'BROWN',
                type: 'Regular',
                region: 'Northeast',
                auction: 'PA - Manheim Pennsylvania'
            },{
                dateSold: '03/11/2014',
                price: '35000',
                odometer: '14918',
                condition: '2.6',
                engine: '4GT/A',
                color: 'ICE SILV',
                type: ' Lease',
                region: 'Midwest',
                auction: 'IL - Manheim Arena Illinois'
            }
        ];

        this.showingAll(false);
        this.transactions.replace(arr);
    },

    _transactionsChanged: function () {},


    toggleSort: function () {
        console.log('toggleSort');
        var arr = this.transactions.slice(0);


        if (this._sort) {
            arr.sort(function (a,b) {
              if (a.dateSold < b.dateSold)
                 return -1;
              if (a.dateSold > b.dateSold)
                return 1;
              return 0;
            });
        } else {
            arr.sort(function (b,a) {
              if (a.dateSold < b.dateSold)
                 return -1;
              if (a.dateSold > b.dateSold)
                return 1;
              return 0;
            });
        }

        this.transactions.replace(arr);
        this._sort = !this._sort;
    }
});

var App = Falcon.View.extend({
    url: '#app-template',

    initialize: function() {
        this.decoder = new DecoderView();
        this.transactions = new TransactionsView();
    }
});

Falcon.apply(new App);
