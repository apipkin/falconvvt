var express = require('express'),
    exphbs  = require('express3-handlebars'),

    routes = require('./lib/routes'),

    app = express(),
    hbs;

hbs = exphbs.create({
    defaultLayout: 'main',
    partialdDir: [
        'views/partials/'
    ]
});

app.configure(function () {
    app.use(express.urlencoded());
    app.use(express.json());

    app.engine('handlebars', hbs.engine);
    app.set('view engine', 'handlebars');
});

app.locals({
    title: "Faclon VVT"
});

// -- ROUTES --
app.use(express.static('public/'));


app.get('/api/decode/:year?/:make?/:model?/:trim?', routes.api_decode);


app.get('/', routes.home);

// START IT UP
app.listen(3000);
console.log('FalconVVT is listengin on: ' + 3000);
